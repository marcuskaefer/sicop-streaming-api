const app = require("express")();
const http = require("http").Server(app);

const io = require("socket.io")(http);
const Blob = require("cross-blob");

var Usercounter = 0;

app.get("/", function(req, res) {
  res.sendFile(__dirname + "/index.html");
});

app.get("/teste", function(req, res) {
  console.log("testeee");
});

io.on("connection", function(socket) {
  Usercounter = Usercounter + 1;
  io.emit("user", Usercounter);
  console.log("a user is connected");
  socket.on("disconnect", function() {
    Usercounter = Usercounter - 1;
    io.emit("user", Usercounter);
    console.log("user disconnected");
  });

  socket.on("audioMessage", function(msg) {
    console.log(msg);
    io.emit("audioMessage", msg);
  });
});

http.listen(3100, function() {
  console.log("listening to port:3100");
});
